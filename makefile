CPP=g++
CPPFLAGS=-std=c++11 -O2 -DDEBUG -pthread
INCLUDES=-Iincludes
SOURCES=$(shell find src -name "*.cpp")
PROGRAM=scheduler

.PHONY: run build clean

run: build
	time ./$(PROGRAM) > log

build: clean
	$(CPP) $(SOURCES) -o $(PROGRAM) $(CPPFLAGS) $(INCLUDES)

clean:
	rm -rf $(PROGRAM) log
