// #include <string>
#include <random>
#include <functional>
#include <sstream>
#include <iostream>
#include <thread>

#include <unistd.h>

#include "task_manager.hpp"

struct MyFunctor
{
    void operator() () const
    {
        std::cout
            << "Ryuuuu se fez um functor"
            << "["
            << this->n
            << "]"
            << std::endl;
    }

    int n;
};

void print( const std::string &to_print )
{
    std::cout
        << to_print
        << std::endl;
}

int main ()
{
    //Rand
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(3000, 10000);
    //Rand

    task_manager::Scheduler sc;
    
    for ( int n = 0; n < ( 1000 * 1000 ) / 3; ++n )//* 1000
    {
        sc.sched( dis(gen) , std::bind( print, "da hadouken ryuuuu, ele e mauuuu" ) );
        
        sc.sched( dis(gen) , MyFunctor{ n } );
        
        sc.sched( dis(gen) ,  []() {
            std::cout
                << "da hadouken ryuuuu, ele e bandiduu"
                << std::endl;
        } );
    }
    // sc.sched( 2000, MyFunctor{ 2 } );
    // sc.sched( 10000, MyFunctor{ 5 } );
    // sc.sched( 6000, MyFunctor{ 4 } );
    // sc.sched( 3000, MyFunctor{ 3 } );
    // sc.sched( 1000, MyFunctor{ 1 } );

    while ( ! sc.empty() ) {}
    sc.join();
  return 0;
}
