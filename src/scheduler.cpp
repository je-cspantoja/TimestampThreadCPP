#include "scheduler.hpp"

#include <iostream>

namespace task_manager
{
    void Scheduler::sched( int offset_ms, const Task::Callback_t & callback )
    {
        bool was_empty = tasks.empty();
        Task *task = new Task( time::millisecond_t( offset_ms ) , callback ); 
        mtx.lock();
        tasks.push( task );
// #ifdef DEBUG
//         std::cout << time::to_string()
//             << "::To Execute:: "
//             << ( long ) task
//             << "  "
//             << task->to_string()
//             << std::endl;
// #endif
        mtx.unlock();
        if ( was_empty )
        {
            runner = thread( [this] { run(); } );
            runner.detach();
        }
        else
        {
            if ( task == tasks.top() )
            {
                worker.changed_work_time();
            }
        }
    }

    void Scheduler::join()
    {
        if ( runner.joinable() )
            runner.join();
    }

    time::ptime Scheduler::first_timeout()
    {
        return tasks.top()->expiration();
    }

    bool Scheduler::empty()
    {
        return tasks.empty();
    }

    void Scheduler::run()
    {
        while ( ! empty() )
        {   
            time::ptime wake_up = first_timeout();
            bool work = worker.rest_until_work( wake_up );
            if ( work )
            {
                process_expired_tasks();
            }
        }
    }

    void Scheduler::process_expired_tasks()
    {
        time::ptime now = time::now();
        mtx.lock();
        while( ! tasks.empty() )
        {
            Task *task = tasks.top();
            if( task->expired( now ) )
            {
#ifdef DEBUG
                std::cout
                    << time::to_string()
                    // << "::Executing:: "
                    << ( long ) task
                    << "  "
                    << task->to_string()
                    << std::endl;
#endif
                tasks.pop();
            }
            else
            {
                break;
            }
            task->execute();
            delete task;
        }
        mtx.unlock();
    }
}// namespace task_manager