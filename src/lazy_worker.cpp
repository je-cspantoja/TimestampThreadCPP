#include <chrono>

#include <iostream>
#include <thread>

#include "lazy_worker.hpp"


namespace task_manager
{

    // void Lazy_Worker::rest_until_work( time::ptime wake_time )
    // {
    //     // std::cout << "Next call " << time::to_string(wake_time) << std::endl;
    //     std::this_thread::sleep_until( wake_time );
    //     // std::cout << "Now!!! " << time::to_string() << std::endl;
    // }
    Lazy_Worker::Lazy_Worker()
    {
        lock = unique_lock<mutex>(cv_m);
    }

    void Lazy_Worker::changed_work_time()
    {
        changed = true;
        cv.notify_all();
    }

    bool Lazy_Worker::rest_until_work( time::ptime wake_time )
    {   
        changed = false;
        if( cv.wait_until( lock, wake_time, [this](){return changed == true;} ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }

}
