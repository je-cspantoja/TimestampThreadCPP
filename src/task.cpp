#include "task.hpp"

#include <sstream>
#include <iostream>

namespace task_manager
{
    Task::Task( time::millisecond_t offset, const Callback_t & callback )
        : execute_at( time::now( offset ) )
        , callback( callback )
    {
        /*VOID*/
    }

    bool Task::lesser_priority_than( const Task & other ) const
    {
        return this->execute_at > other.execute_at;
    }

    bool Task::higher_priority_than( const Task & other ) const
    {
        return this->execute_at < other.execute_at;
    }
    
    bool Task::operator < ( const Task & other ) const
    {
        return lesser_priority_than( other );
    }

    bool Task::operator > ( const Task & other ) const
    {
        return higher_priority_than( other );
    }

    bool Task::expired( time::ptime now ) const
    {
        return now > this->execute_at;
    }

    void Task::execute()
    {
        this->callback();
    }

    time::ptime Task::expiration()
    {
        return this->execute_at;
    }

    std::string Task::to_string() const
    {
        std::stringstream ss;
        ss
            << "Task to be executed at: "
            << time::to_string( this->execute_at );
        return ss.str();
    }

}// namespace task_manager