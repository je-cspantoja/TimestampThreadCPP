#include "time.hpp"

#include <ctime>

namespace task_manager
{
namespace time
{	
    ptime now( millisecond_t offset )
	{
	    return std::chrono::system_clock::now() + offset;
    }

    std::string to_string( ptime t )
    {
        std::time_t tt = std::chrono::system_clock::to_time_t ( t );
        std::string ts = std::string( ctime( &tt ) );
        return ts.substr(0, ts.size() - 1 );
    }
    
    std::string to_string()
	{
	    return to_string( now() );
    }
}// namespace task_manager::time
}// namespace task_manager