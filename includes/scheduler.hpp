#pragma once

#include <queue>
#include <vector>
#include <mutex> 
#include <thread>

#include "task.hpp"
#include "time.hpp"
#include "lazy_worker.hpp"

namespace task_manager
{   
    using std::thread;
    using std::mutex;

    class Scheduler
    {
        public:
            void sched( int offset_ms, const Task::Callback_t & );
            bool empty();
            void run();
            void process_expired_tasks();
            void join();

        private:
            typedef std::priority_queue< Task *, std::vector< Task * >, TaskPointerPriorityComparator > Tasks;
            Tasks tasks;
            mutex mtx;
            Lazy_Worker worker;
            thread runner;
            time::ptime first_timeout();
    };
}// namespace task_manager
