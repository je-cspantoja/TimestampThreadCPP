#pragma once

#include <atomic>
#include <condition_variable>

#include "time.hpp"

namespace task_manager
{
    using std::condition_variable;
    using std::mutex;
    using std::atomic;
    using std::unique_lock;

    class Lazy_Worker
    {
        public:
            Lazy_Worker();
            void changed_work_time();
            bool rest_until_work( time::ptime wake_time );
        private:
            mutex cv_m;
            unique_lock<mutex> lock;
            condition_variable cv;
            atomic<int> changed{0};
    };
}